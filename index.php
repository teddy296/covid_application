    <html>
    <head>
        <title>Carte Covid19</title>
        <link rel="stylesheet" href="assets/css/carte_France.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="js/carte.js"></script>
    </head>

    <body>

    <div class="mapAndInfo">
        <div class="map">

           <?php include('assets/carteFrance.svg'); ?>

        </div>

        <div class="info">
            <div id="nomDuDpt"></div>
            <div id="date"></div>
            <div id="hospit"></div>
            <div id="deces"></div>
            <div id="gueris"></div>
        </div>
    </div>



    </body>
    </html>
