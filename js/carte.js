  //fonction clic de la carte

    $(function () {

        let idRange = $('[id]');
        $(idRange)
            .filter(function() {
                return this.id.match(/^(0|[1-9][0-9]?|100)$/);
            });
        let allDpt = $(idRange);//ensemble des departements

        allDpt.on('click', function() {


            let deptLoc = $(this);//la situation du departement sur la carte (geographique)
            let departementId = deptLoc[0].id;

            //console.log(idRange)

            //fonction d'affichage des données



           let callBackGetSuccess = function(data)
            {
                    //console.log('données api', data, departementId);
                    //alert('nbre de décès à ce jour : ' + data.LiveDataByDepartement[0].date);
                    $('#date').append('date : ' + data.allLiveFranceData[departementId].date);
                    $('#deces').append('morts : ' + data.allLiveFranceData[departementId].deces);
                    $('#hospit').append('hospitalisés : ' + data.allLiveFranceData[departementId].hospitalises);
                    $('#gueris').append('guéris : ' + data.allLiveFranceData[departementId].gueris);
                    $('#nomDuDpt').append('Departement : ' + data.allLiveFranceData[departementId].nom);
                };


            /**lors du clic, on veut:
            1- que toutes les régions se mettent en blanc
            2- que la région selectionnée se mette dans la couleur choisie(rouge par exemple)*/

            allDpt.css('fill', '#fff');
            deptLoc.css('fill', '#f11919');


            let url = "https://coronavirusapi-france.now.sh/AllLiveData";
            $.get(url, callBackGetSuccess)
                .done(function() {
                })
                .fail(function() {
                    alert('error');
                })
                .always(function() {

                });


         });

    });




